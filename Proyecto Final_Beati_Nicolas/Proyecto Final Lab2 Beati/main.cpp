#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>

using namespace std;

#include "rlutil.h"
#include "func_varias.h"
#include "empresa.h"
#include "func_empresa.h"
#include "cereal.h"
#include "func_cereal.h"
#include "exporta.h"
#include "func_exporta.h"
#include "configuracion.h"
#include "reporte.h"

using namespace rlutil;



int main()
{

    int opcion;
    const int esquinaX=5;
    const int esquinaY=2;
    while(true)
    {
        setBackgroundColor(GREY);
        setColor(BLACK);
        system("cls");
        recuadro(esquinaX,esquinaY,55,15);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"MENU PRINCIPAL"<<endl;
        linea(esquinaX+1, esquinaY+3, 55);
        gotoxy(esquinaX+2,esquinaY+4);
        cout<<"1- MENU EMPRESAS";
        gotoxy(esquinaX+2,esquinaY+5);
        cout<<"2- MENU CEREALES";
        gotoxy(esquinaX+2,esquinaY+6);
        cout<<"3- MENU EXPORTACIONES";
        gotoxy(esquinaX+2,esquinaY+7);
        cout<<"4- REPORTES";
        gotoxy(esquinaX+2,esquinaY+8);
        cout<<"5- CONFIGURACION";
        gotoxy(esquinaX+2,esquinaY+9);
        cout<<"0- FIN DEL PROGRAMA"<<endl;
        linea(esquinaX+1, esquinaY+10, 55);
        gotoxy(esquinaX+2,esquinaY+11);
        cout<<"SELECCIONE UNA DE LAS OPCIONES: ";
        cin>>opcion;
        switch(opcion)
        {
            case 1:
                menuEmpresas(esquinaX, esquinaY);
                break;
           case 2:
                menuCereales(esquinaX, esquinaY);
                break;
             case 3:
                menuExportaciones(esquinaX, esquinaY);
                break;
            case 4:
                menuReporte(esquinaX, esquinaY);
                break;
            case 5:
                menuConfiguracion(esquinaX, esquinaY);
                break;
            case 0:
                gotoxy(esquinaX+2,esquinaY+16);
                return 0;
                break;
            default:
                gotoxy(esquinaX+2,esquinaY+12);
                cout<<"OPCION INCORRECTA";
                gotoxy(esquinaX+2,esquinaY+13);
                system("pause");
                break;
        }
    }
    return 0;
}
