#ifndef FUNC_VARIAS_H_INCLUDED
#define FUNC_VARIAS_H_INCLUDED

void cargarCadena(char *pal, int tam){
  int i;
  fflush(stdin);
  for(i=0;i<tam;i++){
      pal[i]=cin.get();
	  if(pal[i]=='\n') break;
	  }
  pal[i]='\0';
  fflush(stdin);
}

//////////////////////////
class Fecha{
    private:
        int dia, mes, anio;
    public:
        Fecha(int d=0, int m=0, int a=0){
            dia=d;
            mes=m;
            anio=a;
        }
        void Mostrar();
        bool Cargar(int , int );
        ///gets()
        int getDia(){return dia;}
        int getMes(){return mes;}
        int getAnio(){return anio;}
        ///sets()
        void setDia(int d){dia=d;}
        void setMes(int m){mes=m;}
        void setAnio(int a){anio=a;}

};

void Fecha::Mostrar(){
    cout<<dia<<"/"<<mes<<"/"<<anio;
}

bool Fecha::Cargar(int x, int y){
    int d, m, a;
    gotoxy(x,y);
    cout<<"DIA: ";
    cin>>d;
    if(d>=1 && d<=31){
        gotoxy(x,y+1);
        cout<<"MES: ";
        cin>>m;
        if(m>=1 && m<=12){
            gotoxy(x,y+2);
            cout<<"ANIO: ";
            cin>>a;
            if(a>=1970 && a<=2022){
                setAnio(a);
                setMes(m);
                setDia(d);
                return true;
                }
            }
        }
    return false;
}
//////////////////////////

int mostrarProvincia(int pos){
    FILE *p;
    int leyo=0;
    char prov[32];
    p=fopen("provincias.dat", "rb");
    if(p==NULL)return -1;
    fseek(p, sizeof(prov)*(pos-1), 0);
    leyo=fread(prov, sizeof(prov) ,1, p);
    fclose(p);
    if(leyo==1) cout<<prov;
    return leyo;


}

/////////////////////////

void gotoxy(int x , int y);

void linea(int x0, int y0, int lineas){
            int z;
            gotoxy(x0,y0);
            for(z=1;z<lineas;z++)cout<<(char)205;
}

void recuadro(int iniX, int iniY, int ancho, int alto){
    int i, j;
    for(i=iniX; i<=iniX+ancho; i++){
        for(j=iniY; j<=iniY+alto; j++){
            gotoxy(i, j);

            //Arriba izquierda
            if(i==iniX && j==iniY){
                cout << (char) 201;
            }
            //Arriba derecha
            else if(i==iniX+ancho && j==iniY){
                cout << (char) 187;
            }
            //Abajo izquierda
            else if(i==iniX && j==iniY+alto){
                cout << (char) 200;
            }
            //Abajo derecha
            else if(i==iniX+ancho && j==iniY+alto){
                cout << (char) 188;
            }
            //Lineas arriba y abajo
            else if(j==iniY || j==iniY+alto){
                cout << (char) 205;
            }
            //Lineas izquierda y derecha
            else if(i==iniX || i==iniX+ancho){
                cout << (char) 186;
            }
            //Dentro del recuadro
            else{
                cout << "";
            }
        }
    }
}

#endif // FUNCIONES_H_INCLUDED
