#ifndef EXPORTA_H_INCLUDED
#define EXPORTA_H_INCLUDED

class Exporta{
    private:
        int idExporta;
        char cuitEmpresa[12];
        char codigoCereal[5];
        int cantToneladas;
        float importe;
        int destino;
        Fecha fechaExporta;
        bool estado;
    public:
        void Mostrar(int , int );

        ///gets()
        int getIdExporta(){return idExporta;}
        const char *getCuitEmpresa(){return cuitEmpresa;}
        const char *getCodigoCereal(){return codigoCereal;}
        int getCantToneladas(){return cantToneladas;}
        float getImporte(){return importe;}
        int getDestino(){return destino;}
        Fecha getFechaExporta(){return fechaExporta;}
        bool getEstado(){return estado;}

        ///sets()
        void setIdExporta(int idE){idExporta=idE;}
        void setCuitEmpresa(const char *cE){strcpy(cuitEmpresa, cE);}
        void setCodigoCereal(const char *cC){strcpy(codigoCereal, cC);}
        void setCantToneladas(int t){cantToneladas=t;}
        void setImporte(float i){importe=i;}
        void setDestino(int d){destino=d;}
        void setFechaExporta(Fecha f){fechaExporta=f;}
        void setEstado(bool e){estado=e;}

        int grabarEnDisco();
        int leerDeDisco(int pos);
        int modificarEnDisco(int pos);

};

int Exporta::grabarEnDisco(){
    FILE *p;
    int escribio;
    p=fopen("exportaciones.dat", "ab");
    if(p==NULL)return -1;
    escribio=fwrite(this, sizeof(Exporta) ,1, p);
    fclose(p);
    return escribio;
}

int Exporta::leerDeDisco(int pos){
    FILE *p;
    int leyo=0;
    p=fopen("exportaciones.dat", "rb");
    if(p==NULL)return -1;
    fseek(p, sizeof(Exporta)*pos, 0);
    leyo=fread(this, sizeof(Exporta) ,1, p);
    fclose(p);
    return leyo;
}


int Exporta::modificarEnDisco(int pos){
    FILE *p;
    int escribio;
    p=fopen("exportaciones.dat", "rb+");
    if(p==NULL)return -1;
    fseek(p, sizeof(Exporta)*pos, 0);
    escribio=fwrite(this, sizeof(Exporta) ,1, p);
    fclose(p);
    return escribio;
}


void Exporta::Mostrar(int x, int y){
    if(estado==true){
        gotoxy(x,y);
        cout<<"ID: ";
        cout<<idExporta;
        gotoxy(x,y+1);
        cout<<"CUIT DE EMPRESA: ";
        cout<<cuitEmpresa;
        gotoxy(x,y+2);
        cout<<"CODIGO DE CEREAL: ";
        cout<<codigoCereal;
        gotoxy(x,y+3);
        cout<<"CANTIDAD DE TONELADAS: ";
        cout<<cantToneladas<<" t";
        gotoxy(x,y+4);
        cout<<"IMPORTE: $";
        cout<<importe<<endl;
        gotoxy(x,y+5);
        cout<<"DESTINO: ";
        mostrarProvincia(destino);
        gotoxy(x,y+6);
        cout<<"FECHA DE LA VENTA: ";
        fechaExporta.Mostrar();
        }
}


#endif // EXPORT_H_INCLUDED
