#ifndef CONFIGURACION_H_INCLUDED
#define CONFIGURACION_H_INCLUDED

///PROTOTIPOS
int menuConfiguracion();
bool backupEmpresas();
bool backupCereales();
bool backupExporta();
bool restaurarEmpresas();
bool restaurarCereales();
bool restaurarExporta();
///

int menuConfiguracion(const int esquinaX, const int esquinaY){
        int opcion;
        while(true) {
            system("cls");
            recuadro(esquinaX,esquinaY,55,15);
            gotoxy(esquinaX+2,esquinaY+2);
            cout<<"MENU CONFIGURACION";
            linea(esquinaX+1, esquinaY+3, 55);
            gotoxy(esquinaX+2,esquinaY+4);
            cout<<"1- COPIA DE SEGURIDAD DEL ARCHIVO DE EMPRESAS";
            gotoxy(esquinaX+2,esquinaY+5);
            cout<<"2- COPIA DE SEGURIDAD DEL ARCHIVO DE CEREALES";
            gotoxy(esquinaX+2,esquinaY+6);
            cout<<"3- COPIA DE SEGURIDAD DEL ARCHIVO DE EXPORTACIONES";
            gotoxy(esquinaX+2,esquinaY+7);
            cout<<"4- RESTAURAR EL ARCHIVO DE EMPRESAS";
            gotoxy(esquinaX+2,esquinaY+8);
            cout<<"5- RESTAURAR EL ARCHIVO DE CEREALES";
            gotoxy(esquinaX+2,esquinaY+9);
            cout<<"6- RESTAURAR EL ARCHIVO DE EXPORTACIONES";
            gotoxy(esquinaX+2,esquinaY+10);
            cout<<"0- VOLVER AL MENU PRINCIPAL";
            linea(esquinaX+1, esquinaY+11, 55);
            gotoxy(esquinaX+2,esquinaY+12);
            cout<<"SELECCIONE UNA DE LAS OPCIONES: ";
            cin>>opcion;
            system("cls");
            recuadro(esquinaX,esquinaY,55,15);
            gotoxy(esquinaX+2,esquinaY+2);
            switch(opcion) {
                    case 1:
                        if(backupEmpresas()==true) cout<<"BACKUP REALIZADO!";
                        else cout<<"NO SE PUDO HACER EL BACKUP";
                        break;
                    case 2:
                        if(backupCereales()==true) cout<<"BACKUP REALIZADO!";
                        else cout<<"NO SE PUDO HACER EL BACKUP";
                        break;
                    case 3:
                        if(backupExporta()==true) cout<<"BACKUP REALIZADO!";
                        else cout<<"NO SE PUDO HACER EL BACKUP";
                        break;
                    case 4:
                        if(restaurarEmpresas()==true) cout<<"RESTAURACION REALIZADA!";
                        else cout<<"NO SE PUDO HACER LA RESTAURACION";
                        break;
                    case 5:
                        if(restaurarCereales()==true) cout<<"RESTAURACION REALIZADA!";
                        else cout<<"NO SE PUDO HACER LA RESTAURACION";
                        break;
                    case 6:
                        if(restaurarExporta()==true) cout<<"RESTAURACION REALIZADA!";
                        else cout<<"NO SE PUDO HACER LA RESTAURACION";
                        break;
                    case 0:
                        return 0;
                        break;
                    default:
                        cout<<"OPCION INCORRECTA";
                        break;
                }
        gotoxy(esquinaX+2,esquinaY+13);
        system("pause");
        }

}


bool backupEmpresas(){ /// OPC 1
    FILE *p;
    p=fopen("empresas.bkp", "wb");
    if(p==NULL) return false;
    Empresa reg;
    int pos=0;
    while(reg.leerDeDisco(pos)==1){
        fwrite(&reg, sizeof (Empresa), 1, p);
        pos++;
    }
    fclose(p);
    if(pos==0)return false;
    return true;
}

bool backupCereales(){ /// OPC 2
    FILE *p;
    p=fopen("cereales.bkp", "wb");
    if(p==NULL) return false;
    Cereal reg;
    int pos=0;
    while(reg.leerDeDisco(pos)==1){
        fwrite(&reg, sizeof (Cereal), 1, p);
        pos++;
    }
    fclose(p);
    if(pos==0)return false;
    return true;
}

bool backupExporta(){ /// OPC 3
    FILE *p;
    p=fopen("exportaciones.bkp", "wb");
    if(p==NULL) return false;
    Exporta reg;
    int pos=0;
    while(reg.leerDeDisco(pos)==1){
        fwrite(&reg, sizeof (Exporta), 1, p);
        pos++;
    }
    fclose(p);
    if(pos==0)return false;
    return true;
}

bool restaurarEmpresas(){ ///OPC 4
    FILE *pc;
    pc=fopen("empresas.bkp", "rb");
    if(pc==NULL) return false;
    Empresa emp;
    bool leyo=0;
    FILE *paux;
    paux=fopen("empresas.dat", "wb");
    if(paux==NULL)return false;
    while(fread(&emp, sizeof (Empresa), 1, pc)==1){
        fwrite(&emp, sizeof(Empresa) ,1, paux);
        leyo=1;
        }
    fclose(paux);
    fclose(pc);
    if(leyo==0)return false;
    return true;
}

bool restaurarCereales(){ ///OPC 5
    FILE *pa, *paux;
    pa=fopen("cereales.bkp", "rb");
    if(pa==NULL) return false;
    Cereal cer;
    bool leyo=0;
    paux=fopen("cereales.dat", "wb");
    if(paux==NULL)return false;
    while(fread(&cer, sizeof (Cereal), 1, pa)==1){
        fwrite(&cer, sizeof(Cereal) ,1, paux);
        leyo=1;
        }
    fclose(paux);
    fclose(pa);
    if(leyo==0)return false;
    return true;
}

bool restaurarExporta(){ ///OPC 6
    FILE *pv, *paux;
    pv=fopen("exportaciones.bkp", "rb");
    if(pv==NULL) return false;
    Exporta exp;
    bool leyo=0;
    paux=fopen("exportaciones.dat", "wb");
    if(paux==NULL)return false;
    while(fread(&exp, sizeof (Exporta), 1, pv)==1){
        fwrite(&exp, sizeof(Exporta) ,1, paux);
        leyo=1;
        }
    fclose(paux);
    fclose(pv);
    if(leyo==0)return false;
    return true;
}


#endif // CONFIGURACION_H_INCLUDED
