#ifndef CEREAL_H_INCLUDED
#define CEREAL_H_INCLUDED

class Cereal{
    private:
        char codigo[5];
        char nombre[30];
        int tipo;
        float precio;
        bool estado;
    public:
        void Cargar(char*,int , int );
        void Mostrar(int , int );
        ///gets()
        const char *getCodigo(){return codigo;}
        const char *getNombre(){return nombre;}
        int getTipo(){return tipo;}
        float getPrecio(){return precio;}
        bool getEstado(){return estado;}

        ///sets()
        void setCodigo(const char *c){strcpy(codigo, c);}
        void setNombre(const char *n){strcpy(nombre, n);}
        void setTipo(int t){if(t>0 && t<4)tipo=t;}
        void setPrecio(float p){if(p>0)precio=p;}
        void setEstado(bool e){estado=e;}

        int grabarEnDisco();
        int leerDeDisco(int pos);
       /* int leerDeDisco2(int pos);*/ //PARA CORROBORAR ARCHIVO CEREALES SIN GLUTEN
        int modificarEnDisco(int pos);

};


int Cereal::grabarEnDisco(){
    FILE *p;
    int escribio;
    p=fopen("cereales.dat", "ab");
    if(p==NULL)return -1;
    escribio=fwrite(this, sizeof(Cereal) ,1, p);
    fclose(p);
    return escribio;
}

int Cereal::leerDeDisco(int pos){
    FILE *p;
    int leyo=0;
    p=fopen("cereales.dat", "rb");
    if(p==NULL)return -1;
    fseek(p, sizeof(Cereal)*pos, 0);
    leyo=fread(this, sizeof(Cereal) ,1, p);
    fclose(p);
    return leyo;
}

/*int Cereal::leerDeDisco2(int pos){ ///LEER EL ARCHIVO SIN GLUTEN
    FILE *p;
    int leyo=0;
    p=fopen("cerealesSinGluten.dat", "rb");
    if(p==NULL)return -1;
    fseek(p, sizeof(Cereal)*pos, 0);
    leyo=fread(this, sizeof(Cereal) ,1, p);
    fclose(p);
    return leyo;
}*/


int Cereal::modificarEnDisco(int pos){
    FILE *p;
    int escribio;
    p=fopen("cereales.dat", "rb+");
    if(p==NULL)return -1;
    fseek(p, sizeof(Cereal)*pos, 0);
    escribio=fwrite(this, sizeof(Cereal) ,1, p);
    fclose(p);
    return escribio;
}


void Cereal::Cargar(char *c, int x, int y){
    int t, p;
    setCodigo(c);
    gotoxy(x,y+1);
    cout<<"INGRESE EL NOMBRE DEL CEREAL: ";
    cin>>nombre;
    gotoxy(67,y-3);
    cout<<"1. CON GLUTEN";
    gotoxy(67,y-2);
    cout<<"2. SIN GLUTEN";
    gotoxy(x,y+3);
    cout<<"INGRESE EL TIPO (1 o 2): ";
    cin>>t;
    while(t<1 || t>2){
        gotoxy(x,y+4);
        cout<<"INGRESE UNO DE LOS 2 TIPOS: ";
        cin>>t;
        }
    setTipo(t);
    gotoxy(x,y+5);
    cout<<"INGRESE EL PRECIO: ";
    cin>>p;
    while(p<=0){
        gotoxy(x,y+6);
        cout<<"INGRESE UN PRECIO POSITIVO: ";
        cin>>p;
        }
    setPrecio(p);
    estado=true;
}

void Cereal::Mostrar(int x, int y){
    if(estado==true){
    gotoxy(x,y);
    cout<<"CODIGO: ";
    cout<<codigo;
    gotoxy(x,y+1);
    cout<<"NOMBRE: ";
    cout<<nombre;
    gotoxy(x,y+2);
    cout<<"TIPO: ";
    if(tipo==1){cout<<"Con gluten";}
    else {cout<<"Sin gluten";}
    gotoxy(x,y+3);
    cout<<"PRECIO: $";
    cout<<precio;
    }
}


#endif // PRODUCTO_H_INCLUDED
