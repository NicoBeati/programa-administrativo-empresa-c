#ifndef EMPRESA_H_INCLUDED
#define EMPRESA_H_INCLUDED


class Empresa{
private:
    char CUIT[12];
    char nombre[30];
    char direccion[25];
    char telefono[15];
    char email[30];
    char nombreContacto[30];
    int provincia;
    bool estado;
public:
    void Cargar(char*, int, int);
    void Mostrar(int , int );
    ///gets()
    const char *getCUIT(){return CUIT;}
    const char *getNombre(){return nombre;}
    const char *getDireccion(){return direccion;}
    const char *getTelefono(){return telefono;}
    const char *getEmail(){return email;}
    const char *getNombreContacto(){return nombreContacto;}
    int getProvincia(){return provincia;}
    bool getEstado(){return estado;}

    ///sets()
    void setCUIT(const char *par){strcpy(CUIT, par);}
    void setNombre(const char *par){strcpy(nombre, par);}
    void setDireccion(const char *par){strcpy(direccion, par);}
    void setTelefono(const char *par){strcpy(telefono,par);}
    void setEmail(const char *par){strcpy(email, par);}
    void setNombreContacto(const char *par){strcpy(nombreContacto, par);}
    void setProvincia(int p){provincia=p;}
    void setEstado(bool e){estado=e;}

    int grabarEnDisco();
    int leerDeDisco(int pos);
    int modificarEnDisco(int pos);
};


int Empresa::grabarEnDisco(){
    FILE *p;
    int escribio;
    p=fopen("empresas.dat", "ab");
    if(p==NULL)return -1;
    escribio=fwrite(this, sizeof(Empresa) ,1, p);
    fclose(p);
    return escribio;
}

int Empresa::leerDeDisco(int pos){
    FILE *p;
    int leyo=0;
    p=fopen("empresas.dat", "rb");
    if(p==NULL)return -1;
    fseek(p, sizeof(Empresa)*pos, 0);
    leyo=fread(this, sizeof(Empresa) ,1, p);
    fclose(p);
    return leyo;
}


int Empresa::modificarEnDisco(int pos){
    FILE *p;
    int escribio;
    p=fopen("empresas.dat", "rb+");
    if(p==NULL)return -1;
    fseek(p, sizeof(Empresa)*pos, 0);
    escribio=fwrite(this, sizeof(Empresa) ,1, p);
    fclose(p);
    return escribio;
}


void Empresa::Mostrar(int x, int y){
    if(estado==true){
        gotoxy(x,y);
        cout<<"CUIT: ";
        cout<<CUIT;
        gotoxy(x,y+1);
        cout<<"NOMBRE: ";
        cout<<nombre;
        gotoxy(x,y+2);
        cout<<"DIRECCION: ";
        cout<<direccion;
        gotoxy(x,y+3);
        cout<<"TELEFONO: ";
        cout<<telefono;
        gotoxy(x,y+4);
        cout<<"EMAIL: ";
        cout<<email;
        gotoxy(x,y+5);
        cout<<"NOMBRE CONTACTO: ";
        cout<<nombreContacto;
        gotoxy(x,y+6);
        cout<<"PROVINCIA: ";
        mostrarProvincia(provincia);

    }
}


void Empresa::Cargar(char *c, int x, int y){
    char n[30], d[25], nc[30];
    int p;
    setCUIT(c);
    gotoxy(x,y);
    cout<<"INGRESE EL NOMBRE: ";
    cargarCadena(n,29);
    setNombre(n);
    gotoxy(x,y+1);
    cout<<"INGRESE LA DIRECCION: ";
    cargarCadena(d,24);
    setDireccion(d);
    gotoxy(x,y+2);
    cout<<"INGRESE EL TELEFONO: ";
    cin>>telefono;
    gotoxy(x,y+3);
    cout<<"INGRESE EL EMAIL: ";
    cin>>email;
    gotoxy(x,y+4);
    cout<<"INGRESE EL NOMBRE DE CONTACTO: ";
    cargarCadena(nc,29);
    setNombreContacto(nc);
    /////////////////////////////
    for (int i=1;i<25;i++){     /// LISTAR LAS PROVINCIAS EN EL CUADRO IZQUIERDO
        gotoxy(67,3+i);
        cout<<i<<". ";
        mostrarProvincia(i);
        }
    ////////////////////////////
    gotoxy(x,y+5);
    cout<<"INGRESE LA PROVINCIA (1 a 24): ";
    cin>>p;
    while(p<1 || p>24){
        gotoxy(x,y+6);
        cout<<"INGRESE UNA PROVINCIA VALIDA: ";
        cin>>p;
        }
    setProvincia(p);
    estado=true;
}

#endif // EMPRESA_H_INCLUDED
