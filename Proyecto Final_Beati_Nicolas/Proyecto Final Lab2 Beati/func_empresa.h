#ifndef FUNC_EMPRESA_H_INCLUDED
#define FUNC_EMPRESA_H_INCLUDED

///PROTOTIPOS
void agregarEmpresa(const int, const int);
void listarEmpresa(const int, const int);
int buscarEmpresaCUIT(char *, const int, const int);
void listarEmpresaCUIT(const int, const int);
void modificarContacto(const int, const int);
void borrarEmpresa(const int, const int);
///

int menuEmpresas(const int esquinaX, const int esquinaY){
    int opcion;
    while(true)
    {
        system("cls");
        recuadro(esquinaX,esquinaY,55,15);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"MENU EMPRESA";
        linea(esquinaX+1, esquinaY+3, 55);
        gotoxy(esquinaX+2,esquinaY+4);
        cout<<"1- AGREGAR EMPRESA";
        gotoxy(esquinaX+2,esquinaY+5);
        cout<<"2- LISTAR TODAS LAS EMPRESAS";
        gotoxy(esquinaX+2,esquinaY+6);
        cout<<"3- LISTAR EMPRESAS POR CUIT";
        gotoxy(esquinaX+2,esquinaY+7);
        cout<<"4- MODIFICAR NOMBRE DE CONTACTO";
        gotoxy(esquinaX+2,esquinaY+8);
        cout<<"5- ELIMINAR EMPRESA";
        gotoxy(esquinaX+2,esquinaY+9);
        cout<<"0- VOLVER AL MENU PRINCIPAL";
        linea(esquinaX+1, esquinaY+10, 55);
        gotoxy(esquinaX+2,esquinaY+11);
        cout<<"SELECCIONE UNA DE LAS OPCIONES: ";
        cin>>opcion;
        system("cls");
        switch(opcion)
        {
            case 1:
                agregarEmpresa(esquinaX, esquinaY);
                break;
            case 2:
                listarEmpresa(esquinaX, esquinaY);
                break;
            case 3:
                listarEmpresaCUIT(esquinaX, esquinaY);
                break;
            case 4:
                modificarContacto(esquinaX, esquinaY);
                break;
            case 5:
                borrarEmpresa(esquinaX, esquinaY);
                break;
            case 0:
                return 0;
                break;
            default:
                recuadro(esquinaX,esquinaY,55,15);
                gotoxy(esquinaX+2,esquinaY+6);
                cout<<"OPCION INCORRECTA";
                gotoxy(esquinaX+2,esquinaY+13);
                system("pause");
                break;
        }
    }
}
///////

int buscarEmpresaCUIT(char *c, const int x, const int y){
    Empresa reg;
    int pos=0;
    while(reg.leerDeDisco(pos)==1){
            if(strcmp(c, reg.getCUIT())==0){
                if(reg.getEstado()==true){
                    return pos;///Existe. No est� borrado
                    }
                else{
                    gotoxy(x,y);
                    cout<<"ESE CUIT FUE BORRADO ANTERIORMENTE";
                    return -2;
                    }
                }
            pos++;
    }
    return -1;
}


void agregarEmpresa(const int esquinaX, const int esquinaY){ ///OPC 1
    char cuit[12];
    Empresa reg;
    int valor;
    recuadro(esquinaX,esquinaY,55,15);
    recuadro(65,esquinaY,40,26); ///RECUADRO IZQUIERDO
    gotoxy(esquinaX+2,esquinaY+2);
    cout<<"AGREGAR EMPRESA";
    linea(esquinaX+1, esquinaY+3, 55);
    gotoxy(esquinaX+2,esquinaY+4);
    cout<<"INGRESE CUIT: ";
    cargarCadena(cuit, 11);
    int encontro=buscarEmpresaCUIT(cuit,esquinaX+2,esquinaY+6);
    if(encontro==-1){
        reg.Cargar(cuit,esquinaX+2,esquinaY+5);
        valor=reg.grabarEnDisco();
        system("cls");
        recuadro(esquinaX,esquinaY,55,15);
        gotoxy(esquinaX+2,esquinaY+2);
        if(valor==1) cout<<"REGISTRO EXITOSO!";
        else{cout<<"ERROR AL GRABAR EN DISCO";}
        }
    if(encontro>=0){
        gotoxy(esquinaX+2,esquinaY+6);
        cout<<"EL CUIT YA EXISTE EN EMPRESAS";
        }
    gotoxy(esquinaX+2,esquinaY+13);
    system("pause");
}


void listarEmpresa(const int esquinaX, const int esquinaY){ ///OPC 2
    Empresa reg;
    bool leyo=false;
    int pos=0, cont=0, x=esquinaX+2, y=esquinaY+2;
    recuadro(esquinaX,esquinaY,55,27);
    while(reg.leerDeDisco(pos)==1){
            reg.Mostrar(x,y);
            if (reg.getEstado()==true){
                    cont++;
                    y+=8;
                    }
            leyo=true;
            pos++;
            if(cont!=0 && cont%3==0){
                gotoxy(esquinaX+2,esquinaY+26);
                system("pause");
                system("cls");
                recuadro(esquinaX,esquinaY,55,27);
                y=esquinaY+2;
                }
            }
    gotoxy(esquinaX+2,esquinaY+2);
    if(leyo==false) cout<<"NO HAY REGISTRO PARA LISTAR";
    gotoxy(esquinaX+2,esquinaY+26);
    system("pause");
}

void listarEmpresaCUIT(const int esquinaX, const int esquinaY){ ///OPC 3
    char cuit[12];
    int pos;
    Empresa obj;
    recuadro(esquinaX,esquinaY,55,15);
    linea(esquinaX+1, esquinaY+3, 55);
    gotoxy(esquinaX+2,esquinaY+2);
    cout<<"INGRESE EL CUIT DE LA EMPRESA: ";
    cargarCadena(cuit, 11);
    pos=buscarEmpresaCUIT(cuit,esquinaX+2,esquinaY+4);
    if(pos>=0){
        system("cls");
        obj.leerDeDisco(pos);
        recuadro(esquinaX,esquinaY,55,15);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"EMPRESA ENCONTRADA!";
        linea(esquinaX+1, esquinaY+3, 55);
        obj.Mostrar(esquinaX+2,esquinaY+4);
        }
    if(pos==-1){
        gotoxy(esquinaX+2,esquinaY+5);
        cout<<"CUIT O ARCHIVO NO ENCONTRADO";
        }
    gotoxy(esquinaX+2,esquinaY+13);
    system("pause");
}


void modificarContacto(const int esquinaX, const int esquinaY){ ///OPC 4
    int pos;
    Empresa reg;
    char cuit[12], nombre[30];
    recuadro(esquinaX,esquinaY,55,15);
    linea(esquinaX+1, esquinaY+3, 55);
    gotoxy(esquinaX+2,esquinaY+2);
    cout<<"INGRESE EL CUIT DE LA EMPRESA: ";
    cargarCadena(cuit, 11);
    pos=buscarEmpresaCUIT(cuit,esquinaX+2,esquinaY+4);
    if(pos>=0){
        reg.leerDeDisco(pos);
        system("cls");
        recuadro(esquinaX,esquinaY,55,15);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"CONTACTO ANTERIOR: "<<reg.getNombreContacto();
        linea(esquinaX+1, esquinaY+3, 55);
        gotoxy(esquinaX+2,esquinaY+4);
        cout<<"INGRESE NUEVO CONTACTO: ";
        cargarCadena(nombre, 29);
        reg.setNombreContacto(nombre);
        reg.modificarEnDisco(pos);
        gotoxy(esquinaX+2,esquinaY+6);
        cout<<"MODIFICACION EXITOSA!";
        }
    if(pos==-1){
        gotoxy(esquinaX+2,esquinaY+4);
        cout<<"CUIT O ARCHIVO NO ENCONTRADO";
        }
    gotoxy(esquinaX+2,esquinaY+13);
    system("pause");
}


void borrarEmpresa(const int esquinaX, const int esquinaY){ ///OPC 5
        char CUIT[12];
        int pos;
        Empresa reg;
        recuadro(esquinaX,esquinaY,55,15);
        linea(esquinaX+1, esquinaY+3, 55);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"INGRESE EL CUIT DE LA EMPRESA: ";
        cargarCadena(CUIT, 11);
        pos=buscarEmpresaCUIT(CUIT,esquinaX+2,esquinaY+4);
        if(pos>=0) {
            reg.leerDeDisco(pos);
            reg.setEstado(false);
            reg.modificarEnDisco(pos);
            gotoxy(esquinaX+2,esquinaY+4);
            cout<<"BORRADO EXITOSO!";
            }
        if(pos==-1){
            gotoxy(esquinaX+2,esquinaY+4);
            cout<<"CUIT O ARCHIVO NO ENCONTRADO";
            }
        gotoxy(esquinaX+2,esquinaY+13);
        system("pause");
}

#endif // FUNC_EMPRESA_H_INCLUDED
