#ifndef FUNC_CEREAL_H_INCLUDED
#define FUNC_CEREAL_H_INCLUDED

///PROTOTIPOS
int buscarCerealCodigo(char *,const int, const int);
void agregarCereal(const int, const int);
void listarCereales(const int, const int);
void listarCerealesCodigo(const int, const int);
void borrarCereal(const int, const int);
void modificarPrecio(const int, const int);
///

int menuCereales(const int esquinaX, const int esquinaY){
    int opcion;
    while(true)
    {
        system("cls");
        recuadro(esquinaX,esquinaY,55,15);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"MENU CEREALES";
        linea(esquinaX+1, esquinaY+3, 55);
        gotoxy(esquinaX+2,esquinaY+4);
        cout<<"1- AGREGAR CEREAL";
        gotoxy(esquinaX+2,esquinaY+5);
        cout<<"2- LISTAR TODOS LOS CEREALES";
        gotoxy(esquinaX+2,esquinaY+6);
        cout<<"3- LISTAR CEREAL POR CODIGO";
        gotoxy(esquinaX+2,esquinaY+7);
        cout<<"4- MODIFICAR PRECIO";
        gotoxy(esquinaX+2,esquinaY+8);
        cout<<"5- ELIMINAR CEREAL";
        gotoxy(esquinaX+2,esquinaY+9);
        cout<<"0- VOLVER AL MENU PRINCIPAL";
        linea(esquinaX+1, esquinaY+10, 55);
        gotoxy(esquinaX+2,esquinaY+11);
        cout<<"SELECCIONE UNA DE LAS OPCIONES: ";
        cin>>opcion;
        system("cls");
        switch(opcion)
        {
            case 1:
                agregarCereal(esquinaX, esquinaY);
                break;
            case 2:
                listarCereales(esquinaX, esquinaY);
                break;
            case 3:
                listarCerealesCodigo(esquinaX, esquinaY);
                break;
            case 4:
                modificarPrecio(esquinaX, esquinaY);
                break;
            case 5:
                 borrarCereal(esquinaX, esquinaY);
                 break;
            case 0:
                return 0;
                break;
            default:
                recuadro(esquinaX,esquinaY,55,15);
                gotoxy(esquinaX+2,esquinaY+6);
                cout<<"OPCION INCORRECTA";
                gotoxy(esquinaX+2,esquinaY+13);
                system("pause");
                break;
        }
    }
}



/////////////////////////////////

int buscarCerealCodigo(char *c, const int x, const int y){
    Cereal reg;
    int pos=0;
    while(reg.leerDeDisco(pos)==1){
            if(strcmp(c, reg.getCodigo())==0){
                if(reg.getEstado()==true){
                    return pos;///Existe. No est� borrado
                    }
                else{
                    gotoxy(x,y);
                    cout<<"ESE CEREAL FUE BORRADO ANTERIORMENTE";
                    return -2;
                    }
                }
            pos++;
    }
    return -1;
}

void agregarCereal(const int esquinaX, const int esquinaY){///OPC 1
    char cod[5];
    Cereal reg;
    int valor;
    recuadro(esquinaX,esquinaY,55,15);
    recuadro(65,esquinaY,40,26); ///RECUADRO IZQUIERDO
    gotoxy(esquinaX+2,esquinaY+2);
    cout<<"AGREGAR CEREAL";
    linea(esquinaX+1, esquinaY+3, 55);
    gotoxy(esquinaX+2,esquinaY+4);
    cout<<"INGRESE CODIGO: ";
    cargarCadena(cod, 4);
    int encontro=buscarCerealCodigo(cod,esquinaX+2,esquinaY+6);
    if(encontro==-1){
        reg.Cargar(cod,esquinaX+2,esquinaY+5);
        valor=reg.grabarEnDisco();
        system("cls");
        recuadro(esquinaX,esquinaY,55,15);
        gotoxy(esquinaX+2,esquinaY+2);
        if(valor==1) cout<<"REGISTRO EXITOSO!";
        else{cout<<"ERROR AL GRABAR EN DISCO";}
        }
    if(encontro>=0){
        gotoxy(esquinaX+2,esquinaY+6);
        cout<<"EL CODIGO YA EXISTE EN CEREALES";
        }
    gotoxy(esquinaX+2,esquinaY+13);
    system("pause");
}


void listarCereales(const int esquinaX, const int esquinaY){ ///OPC 2
    Cereal reg;
    bool leyo=false;
    int pos=0, cont=0, x=esquinaX+2, y=esquinaY+2;
    recuadro(esquinaX,esquinaY,55,26);
    while(reg.leerDeDisco(pos)==1){
            reg.Mostrar(x,y);
            if (reg.getEstado()==true){
                    cont++;
                    y+=5;
                    }
            leyo=true;
            pos++;
            if(cont!=0 && cont%4==0){
                gotoxy(esquinaX+2,esquinaY+24);
                system("pause");
                system("cls");
                recuadro(esquinaX,esquinaY,55,26);
                y=esquinaY+2;
                }
            }
    gotoxy(esquinaX+2,esquinaY+2);
    if(leyo==false) cout<<"NO HAY REGISTRO PARA LISTAR";
    gotoxy(esquinaX+2,esquinaY+24);
    system("pause");
}

void listarCerealesCodigo(const int esquinaX, const int esquinaY){ ///OPC 3
    char cod[5];
    int pos;
    Cereal reg;
    recuadro(esquinaX,esquinaY,55,15);
    linea(esquinaX+1, esquinaY+3, 55);
    gotoxy(esquinaX+2,esquinaY+2);
    cout<<"INGRESE EL CODIGO DEL CEREAL: ";
    cargarCadena(cod,4);
    pos=buscarCerealCodigo(cod,esquinaX+2,esquinaY+4);
    if(pos>=0) {
        system("cls");
        reg.leerDeDisco(pos);
        recuadro(esquinaX,esquinaY,55,15);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"CEREAL ENCONTRADO!";
        linea(esquinaX+1, esquinaY+3, 55);
        reg.Mostrar(esquinaX+2,esquinaY+4);
        }
    if(pos==-1){
        gotoxy(esquinaX+2,esquinaY+5);
        cout<<"CODIGO O ARCHIVO NO ENCONTRADO";
        }
    gotoxy(esquinaX+2,esquinaY+13);
    system("pause");
}

void modificarPrecio(const int esquinaX, const int esquinaY){ ///OPC 4
    int pos;
    Cereal reg;
    float p;
    char cod[5];
    recuadro(esquinaX,esquinaY,55,15);
    linea(esquinaX+1, esquinaY+3, 55);
    gotoxy(esquinaX+2,esquinaY+2);
    cout<<"INGRESE EL CODIGO DEL CEREAL: ";
    cargarCadena(cod, 4);
    pos=buscarCerealCodigo(cod,esquinaX+2,esquinaY+4);
    if(pos>=0){
        reg.leerDeDisco(pos);
        system("cls");
        recuadro(esquinaX,esquinaY,55,15);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"PRECIO ANTERIOR DE "<<reg.getNombre()<<" : $"<<reg.getPrecio();
        linea(esquinaX+1, esquinaY+3, 55);
        gotoxy(esquinaX+2,esquinaY+4);
        cout<<"INGRESE NUEVO PRECIO: $";
        cin>>p;
        gotoxy(esquinaX+2,esquinaY+6);
        if(p>0){
            reg.setPrecio(p);
            reg.modificarEnDisco(pos);
            cout<<"MODIFICACION EXITOSA!";
            }
        else {cout<<"PRECIO INVALIDO";}
        }
    if(pos==-1){
        gotoxy(esquinaX+2,esquinaY+4);
        cout<<"CODIGO O ARCHIVO NO ENCONTRADO";
        }
    gotoxy(esquinaX+2,esquinaY+13);
    system("pause");
}

void borrarCereal(const int esquinaX, const int esquinaY){ ///OPC 5
        char cod[5];
        int pos;
        Cereal reg;
        recuadro(esquinaX,esquinaY,55,15);
        linea(esquinaX+1, esquinaY+3, 55);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"INGRESE EL CODIGO DEL CEREAL: ";
        cargarCadena(cod,4);
        pos=buscarCerealCodigo(cod,esquinaX+2,esquinaY+4);
        if(pos>=0) {
            reg.leerDeDisco(pos);
            reg.setEstado(false);
            reg.modificarEnDisco(pos);
            gotoxy(esquinaX+2,esquinaY+4);
            cout<<"BORRADO EXITOSO!";
            }
        if(pos==-1){
            gotoxy(esquinaX+2,esquinaY+4);
            cout<<"CODIGO O ARCHIVO NO ENCONTRADO";
            }
        gotoxy(esquinaX+2,esquinaY+13);
        system("pause");
}

#endif // FUNC_PRODUCTO_H_INCLUDED
