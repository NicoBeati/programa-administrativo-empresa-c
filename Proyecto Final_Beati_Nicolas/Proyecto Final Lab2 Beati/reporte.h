#ifndef REPORTE_H_INCLUDED
#define REPORTE_H_INCLUDED

///PROTOTIPOS
void listarExportaPorEmpresas(const int, const int);
void mayorDestinoPorAnio(int,const int, const int);
int buscarVectorMayor(int *, int );
bool generarCerealesSinGluten();
//void mostrarSinGluten();
///


int menuReporte(const int esquinaX, const int esquinaY){
        int opcion, anio;
        while(true) {
            system("cls");
            recuadro(esquinaX,esquinaY,65,15);
            gotoxy(esquinaX+2,esquinaY+2);
            cout<<"MENU REPORTES";
            linea(esquinaX+1, esquinaY+3, 65);
            gotoxy(esquinaX+2,esquinaY+4);
            cout<<"1- LISTAR NUMERO DE EXPORTACIONES HECHAS A EMPRESAS";
            gotoxy(esquinaX+2,esquinaY+5);
            cout<<"2- LA PROVINCIA CON MAYOR EXPORTACIONES RECIBIDAS EN ANIO X";
            gotoxy(esquinaX+2,esquinaY+6);
            cout<<"3- GENERAR UN ARCHIVO CON LOS CEREALES LIBRES DE GLUTEN";
            gotoxy(esquinaX+2,esquinaY+7);
            cout<<"0- VOLVER AL MENU PRINCIPAL";
            linea(esquinaX+1, esquinaY+10, 65);
            gotoxy(esquinaX+2,esquinaY+11);
            cout<<"SELECCIONE UNA DE LAS OPCIONES: ";
            cin>>opcion;
            system("cls");
            switch(opcion) {
                    case 1:
                        listarExportaPorEmpresas(esquinaX, esquinaY);
                        break;
                    case 2:
                        recuadro(esquinaX,esquinaY,65,15);
                        linea(esquinaX+1, esquinaY+3, 65);
                        gotoxy(esquinaX+2,esquinaY+2);
                        cout<<"INGRESE ANIO: ";
                        cin>>anio;
                        mayorDestinoPorAnio(anio,esquinaX, esquinaY);
                        break;
                    case 3:
                        recuadro(esquinaX,esquinaY,65,15);
                        gotoxy(esquinaX+2,esquinaY+2);
                        if(generarCerealesSinGluten()==true) cout<<"REGISTROS GUARDADOS!";
                        else {cout<<"NO SE PUDO REALIZAR EL REGISTRO";}
                        gotoxy(esquinaX+2,esquinaY+13);
                        system("pause");
                        break;
                   /* case 9:
                        mostrarSinGluten();
                        break;*/
                    case 0:
                        return 0;
                        break;
                    default:
                        recuadro(esquinaX,esquinaY,65,15);
                        gotoxy(esquinaX+2,esquinaY+6);
                        cout<<"OPCION INCORRECTA";
                        gotoxy(esquinaX+2,esquinaY+13);
                        system("pause");
                        break;
            }
        }
}

void listarExportaPorEmpresas(const int esquinaX, const int esquinaY){ ///OPC 1
    Empresa emp;
    Exporta exp;
    int pos=0, pos2=0, cont, x=esquinaX+2, y=esquinaY+2;
    recuadro(esquinaX,esquinaY,65,27);
    while(emp.leerDeDisco(pos)==1){
        cont=0;
        while(exp.leerDeDisco(pos2)==1){
            if(strcmp(emp.getCUIT(),exp.getCuitEmpresa())==0){
                cont++;
                }
            pos2++;
            }
        gotoxy(x,y);
        cout<<"EMPRESA: ";
        gotoxy(x,y+1);
        cout<<emp.getCUIT();
        gotoxy(x,y+2);
        cout<<emp.getNombre();
        gotoxy(x,y+3);
        cout<<"EXPORTACIONES RECIBIDAS:"<<cont;
        pos++;
        pos2=0;
        y+=6;
        if(pos%4==0){
                gotoxy(esquinaX+2,esquinaY+26);
                system("pause");
                system("cls");
                recuadro(esquinaX,esquinaY,65,27);
                y=esquinaY+2;
                }
        }
    gotoxy(esquinaX+2,esquinaY+26);
    system("pause");
}

void mayorDestinoPorAnio(int a, const int esquinaX, const int esquinaY){ ///OPC 2
    Exporta reg;
    int pos=0, vProv[24]={0}, mayor;
    bool hubo=false;
    while(reg.leerDeDisco(pos)==1){
        if(reg.getEstado()==true && reg.getFechaExporta().getAnio()==a){
            vProv[reg.getDestino()-1]++;
            hubo=true;
        }
        pos++;
    }
    mayor=buscarVectorMayor(vProv, 24);
    gotoxy(esquinaX+2,esquinaY+5);
    if(hubo==true){
        cout<<"DESTINO CON MAYOR EXPORTACIONES EN "<<a<<" FUE: ";
        gotoxy(esquinaX+2,esquinaY+7);
        mostrarProvincia(mayor+1);
        }
    else{cout<<"NO SE REGISTRAN EXPORTACIONES ESE ANIO";}
    gotoxy(esquinaX+2,esquinaY+13);
    system("pause");
}

int buscarVectorMayor(int *v, int tam){
    int i, aux=0 ;
    for(i=1;i<tam;i++){
        if(v[aux]<v[i]){
            aux=i;
        }
    }
    return aux;
}


bool generarCerealesSinGluten(){ ///OPC 3
    Cereal reg;
    int pos=0, escribio;
    FILE *p;
    p=fopen("cerealesSinGluten.dat", "ab");
    if(p==NULL)return false;
    while(reg.leerDeDisco(pos)==1){
        if(reg.getTipo()==2){
           escribio=fwrite(&reg, sizeof(Cereal) ,1, p);
           if(escribio!=1){return false;}
        }
        pos++;
    }
    fclose(p);
    return true;
}


/*void mostrarSinGluten(){ ///////////// MOSTRAR ARCHIVO DE OPC 3
    Cereal reg;
    int leyo=0, pos=0;
    while(reg.leerDeDisco2(pos)==1){
            reg.Mostrar();
            leyo=1;
            pos++;
            }
    if(leyo==1) cout<<"FIN DE LISTADO"<<endl;
    else{cout<<"NO HAY REGISTRO PARA LISTAR"<<endl;}
    system("pause");
}*/


#endif // REPORTE_H_INCLUDED
