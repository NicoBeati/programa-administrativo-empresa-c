#ifndef FUNC_EXPORTA_H_INCLUDED
#define FUNC_EXPORTA_H_INCLUDED

///PROTOTIPOS
int generarIdExporta();
void agregarExporta(const int, const int);
void listarExporta(const int, const int);
int buscarID(int ,const int, const int);
void listarExportaID(const int, const int);
void modificarToneladas(const int, const int);
void borrarExporta(const int, const int);
///

int menuExportaciones(const int esquinaX, const int esquinaY){
    int opcion;
    while(true)
    {
        system("cls");
        recuadro(esquinaX,esquinaY,55,15);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"MENU EXPORTACIONES";
        linea(esquinaX+1, esquinaY+3, 55);
        gotoxy(esquinaX+2,esquinaY+4);
        cout<<"1- AGREGAR EXPORTACION";
        gotoxy(esquinaX+2,esquinaY+5);
        cout<<"2- LISTAR TODAS LAS EXPORTACIONES";
        gotoxy(esquinaX+2,esquinaY+6);
        cout<<"3- LISTAR EXPORTACION POR ID";
        gotoxy(esquinaX+2,esquinaY+7);
        cout<<"4- MODIFICAR TONELADAS";
        gotoxy(esquinaX+2,esquinaY+8);
        cout<<"5- ELIMINAR EXPORTACION";
        gotoxy(esquinaX+2,esquinaY+9);
        cout<<"0- VOLVER AL MENU PRINCIPAL";
        linea(esquinaX+1, esquinaY+10, 55);
        gotoxy(esquinaX+2,esquinaY+11);
        cout<<"SELECCIONE UNA DE LAS OPCIONES: ";
        cin>>opcion;
        system("cls");
        switch(opcion)
        {
            case 1:
                agregarExporta(esquinaX, esquinaY);
                break;
            case 2:
                listarExporta(esquinaX, esquinaY);
                break;
            case 3:
                listarExportaID(esquinaX, esquinaY);
                break;
            case 4:
                modificarToneladas(esquinaX, esquinaY);
                break;
            case 5:
                borrarExporta(esquinaX, esquinaY);
                break;
            case 0:
                return 0;
                break;
            default:
                recuadro(esquinaX,esquinaY,55,15);
                gotoxy(esquinaX+2,esquinaY+6);
                cout<<"OPCION INCORRECTA";
                gotoxy(esquinaX+2,esquinaY+13);
                system("pause");
                break;
        }
    }

}

/////

int generarIdExporta(){
        Exporta reg;
        int pos=0, idNueva;
        while(reg.leerDeDisco(pos)==1){
            pos++;
    }
    idNueva=reg.getIdExporta()+1;
    return idNueva;
}


void agregarExporta(const int x, const int y){ ///OPC 1
    Empresa emp;
    Cereal cer;
    Exporta exp;
    int id, pos, valor;
    bool aux;
    float cant;
    char cod[5], cuit[12];
    Fecha f;
    id=generarIdExporta();
    recuadro(x,y,55,17);
    gotoxy(x+2,y+2);
    cout<<"AGREGAR EXPORTACION";
    linea(x+1, y+3, 55);
    gotoxy(x+2,y+4);
    cout<<"ID DE LA VENTA: "<<id;
    exp.setIdExporta(id);
    gotoxy(x+2,y+5);
    cout<<"INGRESE CUIT DE EMPRESA: ";
    cargarCadena(cuit, 11);
    pos=buscarEmpresaCUIT(cuit,x+2,y+6);
    if(pos>=0){
            emp.leerDeDisco(pos);
            exp.setCuitEmpresa(cuit);
            gotoxy(x+2,y+6);
            cout<<"INGRESE CODIGO DE CEREAL: ";
            cargarCadena(cod, 4);
            pos=buscarCerealCodigo(cod,x+2,y+7);
            if(pos>=0){
                    cer.leerDeDisco(pos);
                    exp.setCodigoCereal(cod);
                    gotoxy(x+2,y+7);
                    cout<<"INGRESE CANTIDAD DE TONELADAS: ";
                    cin>>cant;
                    if(cant>0){
                            exp.setCantToneladas(cant);
                            gotoxy(x+2,y+8);
                            cout<<"IMPORTE: $";
                            exp.setImporte(cant*cer.getPrecio());
                            cout<<exp.getImporte();
                            gotoxy(x+2,y+9);
                            cout<<"DESTINO: ";
                            exp.setDestino(emp.getProvincia());
                            cout<<exp.getDestino();
                            gotoxy(x+2,y+10);
                            cout<<"INGRESE FECHA DE LA EXPORTACION: ";
                            aux=f.Cargar(x+2,y+11);
                            if(aux==true){
                                exp.setFechaExporta(f);
                                exp.setEstado(true);
                                valor=exp.grabarEnDisco();
                                system("cls");
                                recuadro(x,y,55,17);
                                gotoxy(x+2,y+2);
                                if(valor==1) cout<<"REGISTRO EXITOSO!";
                                else{cout<<"ERROR AL GRABAR EN DISCO";}
                                }
                            else {
                                gotoxy(x+2,y+14);
                                cout<<"FECHA INVALIDA";
                                }
                            }
                    else {
                        gotoxy(x+2,y+9);
                        cout<<"CANTIDAD DE TONELADAS INVALIDA";
                        }
                    }
            else {
                gotoxy(x+2,y+8);
                cout<<"CODIGO DE CEREAL INVALIDO";
                }
            }
    else {
        gotoxy(x+2,y+7);
        cout<<"CUIT DE EMPRESA INVALIDO";
        }
    gotoxy(x+2,y+15);
    system("pause");
}

int buscarID(int id, const int x, const int y){
    Exporta reg;
    int pos=0;
    while(reg.leerDeDisco(pos)==1){
            if(reg.getIdExporta()==id){
                if(reg.getEstado()==true){
                    return pos;///Existe. No est� borrado
                    }
                else{
                    gotoxy(x,y);
                    cout<<"ESA EXPORTACION FUE BORRADA ANTERIORMENTE";
                    return -2;
                    }
                }
            pos++;
            }
    return -1;
}

void listarExporta(const int esquinaX, const int esquinaY){ ///OPC 2
    Exporta reg;
    bool leyo=false;
    int pos=0, cont=0, x=esquinaX+2, y=esquinaY+2;
    recuadro(esquinaX,esquinaY,55,27);
    while(reg.leerDeDisco(pos)==1){
            reg.Mostrar(x,y);
            if (reg.getEstado()==true){
                    cont++;
                    y+=8;
                    }
            leyo=true;
            pos++;
            if(cont!=0 && cont%3==0){
                gotoxy(esquinaX+2,esquinaY+26);
                system("pause");
                system("cls");
                recuadro(esquinaX,esquinaY,55,27);
                y=esquinaY+2;
                }
            }
    gotoxy(esquinaX+2,esquinaY+2);
    if(leyo==false) cout<<"NO HAY REGISTRO PARA LISTAR";
    gotoxy(esquinaX+2,esquinaY+26);
    system("pause");
}

void listarExportaID(const int esquinaX, const int esquinaY){ ///OPC 3
    int id, pos;
    Exporta reg;
    recuadro(esquinaX,esquinaY,55,15);
    linea(esquinaX+1, esquinaY+3, 55);
    gotoxy(esquinaX+2,esquinaY+2);
    cout<<"INGRESE EL ID DE LA EXPORTACION: ";
    cin>>id;
    pos=buscarID(id,esquinaX+2,esquinaY+4);
    if(pos>=0) {
        system("cls");
        reg.leerDeDisco(pos);
        recuadro(esquinaX,esquinaY,55,15);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"ID ENCONTRADO!";
        linea(esquinaX+1, esquinaY+3, 55);
        reg.Mostrar(esquinaX+2,esquinaY+4);
        }
    if(pos==-1){
        gotoxy(esquinaX+2,esquinaY+5);
        cout<<"ID O ARCHIVO NO ENCONTRADO";
        }
    gotoxy(esquinaX+2,esquinaY+13);
    system("pause");
}

void modificarToneladas(const int esquinaX, const int esquinaY){ ///OPC 4
    int pos, pos2, id;
    Exporta exp;
    Cereal cer;
    int cant;
    char cod[5];
    recuadro(esquinaX,esquinaY,55,15);
    linea(esquinaX+1, esquinaY+3, 55);
    gotoxy(esquinaX+2,esquinaY+2);
    cout<<"INGRESE EL ID DE LA EXPORTACION: ";
    cin>>id;
    pos=buscarID(id,esquinaX+2,esquinaY+4);
    if(pos>=0){
        exp.leerDeDisco(pos);
        strcpy(cod, exp.getCodigoCereal());
        pos2=buscarCerealCodigo(cod,esquinaX+2,esquinaY+4);
        if(pos2>=0){
            cer.leerDeDisco(pos2);
            system("cls");
            recuadro(esquinaX,esquinaY,55,15);
            gotoxy(esquinaX+2,esquinaY+2);
            cout<<"TONELADAS DE LA EXPORT. "<<id<<": "<<exp.getCantToneladas()<<" t";
            linea(esquinaX+1, esquinaY+3, 55);
            gotoxy(esquinaX+2,esquinaY+4);
            cout<<"INGRESE NUEVO PESO: ";
            cin>>cant;
            gotoxy(esquinaX+2,esquinaY+6);
            if(cant>0){
                exp.setCantToneladas(cant);
                exp.setImporte(cant*cer.getPrecio());
                exp.modificarEnDisco(pos);
                cout<<"MODIFICACION EXITOSA!";
                }
            else {cout<<"PESO INVALIDO";}
            }
        }
    if(pos==-1){
        gotoxy(esquinaX+2,esquinaY+4);
        cout<<"ID O ARCHIVO NO ENCONTRADO";
        }
    if(pos2==-1){
        gotoxy(esquinaX+2,esquinaY+4);
        cout<<"CODIGO DE CEREAL O ARCHIVO NO ENCONTRADO";
        }
    gotoxy(esquinaX+2,esquinaY+13);
    system("pause");
}

void borrarExporta(const int esquinaX, const int esquinaY){ ///OPC 5
        int id, pos;
        Exporta reg;
        recuadro(esquinaX,esquinaY,55,15);
        linea(esquinaX+1, esquinaY+3, 55);
        gotoxy(esquinaX+2,esquinaY+2);
        cout<<"INGRESE EL ID DE LA EXPORTACION: ";
        cin>>id;
        pos=buscarID(id,esquinaX+2,esquinaY+4);
        if(pos>=0) {
            reg.leerDeDisco(pos);
            reg.setEstado(false);
            reg.modificarEnDisco(pos);
            gotoxy(esquinaX+2,esquinaY+4);
            cout<<"BORRADO EXITOSO!";
            }
        if(pos==-1){
            gotoxy(esquinaX+2,esquinaY+4);
            cout<<"ID O ARCHIVO NO ENCONTRADO";
            }
        gotoxy(esquinaX+2,esquinaY+13);
        system("pause");
}

#endif // FUNC_EXPORTA_H_INCLUDED
